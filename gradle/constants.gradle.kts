val kotlinVersion: String by extra {"1.3.11"}
val junitVersion: String by extra {"5.3.2"}
val jacksonVersion: String by extra {"2.9.7"}
val springDataVersion : String by extra{"2.1.3.RELEASE"}
val springBootVersion: String by extra {"2.1.1.RELEASE"}
val springVersion: String by extra {"5.1.3.RELEASE"}
val dokkaVersion: String by extra {"0.9.17"}
val nucleusVersion: String by extra {"0.0.1-Alpha"}
